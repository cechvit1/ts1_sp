import cz.cvut.fel.Login;
import cz.cvut.fel.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;



public class LoginTest {
    private ChromeDriver driver;
    private Main mainPage;
    private Login loginPage;

    @BeforeEach
    void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new Main(driver);
        this.loginPage = new Login(driver);
        Dimension newDimension = new Dimension(1200, 900);
        driver.manage().window().setSize(newDimension);
    }

    @Test
    void loginTest() {
        mainPage.setUp();
        loginPage.setUp("https://top4running.cz/user/login");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        loginPage.login();
        String name = loginPage.getName();

        Assertions.assertEquals(name, "Vít Čech");
    }
}
