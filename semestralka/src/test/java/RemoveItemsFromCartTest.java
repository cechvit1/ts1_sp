import cz.cvut.fel.*;
import org.asynchttpclient.util.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class RemoveItemsFromCartTest {

    private ChromeDriver driver;
    private Main mainPage;
    private Cart cartPage;
    private Login loginPage;
    private RemoveItemsFromCart removeItemsFromCartPage;

    @BeforeEach
    void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new Main(driver);
        this.removeItemsFromCartPage = new RemoveItemsFromCart(driver);
        this.cartPage = new Cart(driver);
        this.loginPage = new Login(driver);
        Dimension newDimension = new Dimension(1200, 900);
        driver.manage().window().setSize(newDimension);
    }

    @Test
    void cartTest() {
        mainPage.setUp();
        loginPage.setUp("https://top4running.cz/user/login");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        loginPage.login();

        removeItemsFromCartPage.setUp("https://top4running.cz/basket/show");
        removeItemsFromCartPage.deleteItem();
        removeItemsFromCartPage.deleteItem();
        boolean emptyCart = removeItemsFromCartPage.isEmpty();

        assertTrue(emptyCart);
    }
}
