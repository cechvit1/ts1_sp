import cz.cvut.fel.Main;
import cz.cvut.fel.Sort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SortItemsTest {

    private ChromeDriver driver;
    private Main mainPage;
    private Sort sortPage;

    @BeforeEach
    void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new Main(driver);
        this.sortPage = new Sort(driver);
        Dimension newDimension = new Dimension(1200, 900);
        driver.manage().window().setSize(newDimension);
    }

    @Test
    public void sortTest(){

        mainPage.setUp();
        sortPage.setUp("https://top4running.cz/c/beh-bezecke-obleceni");
        sortPage.expensive();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean statement = sortPage.getPrice();

        assertTrue(statement);

    }
}
