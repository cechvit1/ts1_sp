import cz.cvut.fel.Main;
import cz.cvut.fel.Search;
import cz.cvut.fel.Sort;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SearchTest {

    private ChromeDriver driver;
    private Main mainPage;
    private Search searchPage;



    @BeforeEach
    void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new Main(driver);
        this.searchPage = new Search(driver);

        Dimension newDimension = new Dimension(1200, 900);
        driver.manage().window().setSize(newDimension);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/file.csv", encoding = "UTF-8", delimiter = ':')
    void searchTest(String item, String brand, String minPrice, String maxPrice) {
        System.out.println(item);
        mainPage.setUp();
//        searchPage.setUp("https://top4running.cz/?q=" + item + "&lb.f%5B%5D=brand%3A" + brand + "&lb.f%5B%5D=price_amount%3A" + minPrice +"%7C"+ maxPrice);
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        String url = "https://top4running.cz/?lb.f%5B%5D=brand%3A"+brand+"&lb.f%5B%5D=price_amount%3A"+minPrice+"%7C"+maxPrice+"&q="+item;
        searchPage.setUp(url);


//        double price = searchPage.getItem();
        System.out.println(url);
        System.out.println(searchPage.getUrl());
//        boolean statement = url == searchPage.getUrl();
//        double min = Double.parseDouble(minPrice);
//        double max = Double.parseDouble(maxPrice);
//        if(min<= price && price <= max){
//            statement = true;
//        }

        Assertions.assertEquals(url, searchPage.getUrl());

    }
}
