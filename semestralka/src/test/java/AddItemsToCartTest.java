import cz.cvut.fel.AddItemsToCart;
import cz.cvut.fel.Cart;
import cz.cvut.fel.Login;
import cz.cvut.fel.Main;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddItemsToCartTest {

    private ChromeDriver driver;
    private Main mainPage;
    private AddItemsToCart addItemsToCart;
    private Cart cart;
    private Login loginPage;

    @BeforeEach
    void setUp() {
        this.driver = new ChromeDriver();
        this.mainPage = new Main(driver);
        this.addItemsToCart = new AddItemsToCart(driver);
        this.cart = new Cart(driver);
        this.loginPage = new Login(driver);
        Dimension newDimension = new Dimension(1200, 900);
        driver.manage().window().setSize(newDimension);
    }


    @Test
    void cartTest() {
        mainPage.setUp();

        loginPage.setUp("https://top4running.cz/user/login");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        loginPage.login();

        addItemsToCart.setUp("https://top4running.cz/p/nike-big-mouth-bottle-2-0-22-oz-n000004240822");
        addItemsToCart.addBottle();
        addItemsToCart.setUp("https://top4running.cz/p/adidas-bp-power-iv-m-dy1970");
        addItemsToCart.addBag();

        cart.setUp("https://top4running.cz/basket/show");
        String bottle =  cart.getBottleName();
        String bag = cart.getBagName();

        Assertions.assertEquals(bottle, "Láhev Nike BIG MOUTH BOTTLE 2.0 - 22 OZ");
        Assertions.assertEquals(bag, "Batoh adidas BP POWER IV M");
    }
}
