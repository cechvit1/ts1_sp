package cz.cvut.fel;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login {
    private ChromeDriver driver;

    public Login(ChromeDriver driver) {this.driver = driver;}

    public void setUp(String url) {
        driver.get(url);
        PageFactory.initElements(driver, this);

    }
    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div/div[4]/div/div/div[1]/ul/li[2]/div/div/div[2]/div[1]")
    private WebElement name;
    @FindBy(xpath = "//*[@id=\"frm-logInForm-login\"]")
    private WebElement userNameInput;

    @FindBy(id = "passField5")
    private WebElement passwordInput;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div/div[4]/div/div/div[2]/form/fieldset/div[3]/div/p[1]/button")
    private WebElement loginButton;


    public void login(){
        userNameInput.sendKeys("vitekcechvc@icloud.com");
        passwordInput.sendKeys("Testovani1");
        loginButton.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getName(){
        return name.getText();
    }

}
