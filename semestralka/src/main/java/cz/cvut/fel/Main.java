package cz.cvut.fel;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Main {
    private final ChromeDriver driver;

    public Main(ChromeDriver driver) {
        this.driver = driver;
    }

    public void setUp() {
        driver.get("https://www.top4running.cz/");
        PageFactory.initElements(driver, this);

        Cookie cookie = new Cookie("w|t4r","W3sidiI6MSwicCI6ImFuYWx5dGljcyIsInB2IjoxLCJhdCI6ImJyb3dzZXIiLCJsIjoiY3MtQ1oiLCJzIjoxLCJjYSI6NzYxNjY0MzF9LHsidiI6MSwicCI6Im1hcmtldGluZyIsInB2IjoxLCJhdCI6ImJyb3dzZXIiLCJsIjoiY3MtQ1oiLCJzIjoxLCJjYSI6NzYxNjY0MzF9XQ==");
        Cookie cookie1 = new Cookie("cbat4r", "eyJjcmVhdGVkQXQiOjE2ODU2MjU3NjN9");
        Cookie cookie2 = new Cookie("_gat_UA-153493-18", "1");
        Cookie cookie3 = new Cookie("_ga_5KC9N70CHX", "GS1.1.1685625722.1.1.1685625763.0.0.0");
        Cookie cookie4 = new Cookie("_ranaCid", "790071314.1685625763");
        Cookie cookie5 = new Cookie("_gid", "GA1.2.919770464.1685625763");
        Cookie cookie6 = new Cookie("_ga", "GA1.2.1866201903.1685625756");
        driver.manage().addCookie(cookie);
        driver.manage().addCookie(cookie1);
        driver.manage().addCookie(cookie2);
        driver.manage().addCookie(cookie3);
        driver.manage().addCookie(cookie4);
        driver.manage().addCookie(cookie5);
        driver.manage().addCookie(cookie6);

        driver.get("https://www.top4running.cz/");
    }

    public void setUp(String url) {
        driver.get(url);
    }

    @FindBy(xpath = "/html/body/div[1]/nav/div/div[1]/ul[4]/li[3]/a")
    WebElement loginIcon;

    @FindBy(xpath = "/html/body/div[1]/nav/div/div[1]/ul[4]/li[3]/div/ul/li[1]/a/div/div[2]/div[1]")
    WebElement userName;

    @FindBy(xpath = "//*[@id=\"wlcWrapper\"]/div[2]/div/div[2]/button[1]")
    WebElement cookies;
    public void goToLoginPage() {
        loginIcon.click();
    }

    public String getUser(){return userName.getText();}

    public void acceptCookies(){cookies.click();}
}