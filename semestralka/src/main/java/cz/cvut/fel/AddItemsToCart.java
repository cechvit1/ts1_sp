package cz.cvut.fel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddItemsToCart {
    private ChromeDriver driver;

    public AddItemsToCart(ChromeDriver driver){this.driver = driver;}

    public void setUp(String url){
        driver.get(url);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[2]/div/form/div[8]/div[1]/button/span[1]/span")
    WebElement bottle;

    public void addBottle(){
        bottle.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[2]/div/form/div[8]/div[1]/button/span[1]/span")
    WebElement bag;

    public void addBag(){
        bag.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
