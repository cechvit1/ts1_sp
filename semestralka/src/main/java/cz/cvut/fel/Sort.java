package cz.cvut.fel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Sort {
    private ChromeDriver driver;

    public Sort(ChromeDriver driver) {this.driver = driver;}

    public void setUp(String url) {
        driver.get(url);
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div/div[6]/div[2]/div/div[1]/div[3]/div/div/div[1]/a[3]")
    WebElement expensive;


    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div/div[6]/div[2]/div/div[2]/a[1]/div/div/div[4]/div")
    WebElement price;

    @FindBy(xpath = "/html/body/div[2]/div[2]/div[3]/div/div[6]/div[2]/div/div[2]/a[2]/div/div/div[4]/div")
    WebElement price1;


    public void expensive(){
        expensive.click();
    }

    public boolean getPrice(){
        String priceOfItem = price.getText();
        String[] parts =priceOfItem.split(" ");
        int num1 = Integer.parseInt(parts[0]);
        String priceOfItem1 = price1.getText();
        String[] parts1 =priceOfItem1.split(" ");
        int num2 = Integer.parseInt(parts1[0]);
        return num1 >= num2;

    }

}
