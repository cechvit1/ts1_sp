package cz.cvut.fel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Search {

    private ChromeDriver driver;

    public Search(ChromeDriver driver) {this.driver = driver;}

    public void setUp(String url) {
        driver.get(url);
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "/html/body/div[2]/div[2]/div/div[1]/div/div[2]/main/div[3]/div/div/a/div[2]/div/div[3]/div")
    WebElement price;

    public double getItem(){
        String priceOfItem = price.getText();
        String[] parts =priceOfItem.split(" ");
        double num = Double.parseDouble(parts[0]);
        return num;
    }

    public String getUrl(){
        return driver.getCurrentUrl();
    }
}
