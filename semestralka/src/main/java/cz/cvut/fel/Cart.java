package cz.cvut.fel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Cart {

    private ChromeDriver driver;

    public Cart(ChromeDriver driver) {this.driver = driver;}

    public void setUp(String url) {
        driver.get(url);
        PageFactory.initElements(driver, this);

    }
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[3]/div/div/div[2]/div[1]/div[1]/form/div[1]/div[2]/div/div[2]/div/div[1]/ul/li[1]/a")
    WebElement bagName;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[3]/div/div/div[2]/div[1]/div[1]/form/div[1]/div[3]/div/div[2]/div/div[1]/ul/li[1]/a")
    WebElement bottleName;
    public String getBagName(){
        return bagName.getText();
    }
    public String getBottleName(){
        return bottleName.getText();
    }


}
