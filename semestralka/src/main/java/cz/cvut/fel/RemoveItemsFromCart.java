package cz.cvut.fel;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RemoveItemsFromCart {
    private ChromeDriver driver;

    public RemoveItemsFromCart(ChromeDriver driver){this.driver = driver;}

    public void setUp(String url){
        driver.get(url);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "/html/body/div[1]/div[1]/div[3]/div/div/div[2]/div[1]/div[1]/form/div[1]/div[2]/div/div[2]/div/div[2]/div/div[3]/a/i")
    WebElement delete;

    @FindBy(xpath = "/html/body/div[1]/div[1]/div[3]/div/div/div[2]/div/div/div/div/h5")
    WebElement empty;
    public void deleteItem() {

        delete.click();


    }
    public boolean isEmpty(){
        return empty.getText().equals("V košíku nemáš žádné položky.");
    }
    }



